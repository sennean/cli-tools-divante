<?php
/**
 * Created by PhpStorm.
 * User: karan
 * Date: 06.07.15
 * Time: 17:32
 */
namespace Mapper\Config;
class Config
{
    public static $schema =[
        'ceneo'=>[
            'url'=>'/*/@url',
            'description'=>'/*/desc',
            'name'=>'/*/name'
        ],
        'nokaut'=>[
            'url'=>'//url',
            'description'=>'//description',
            'name'=>'//name'
        ]
    ];

    public static $location =
        [
            'ceneo'=>'/var/www/html/divante/sample_data/ceneo_sample.xml', //ścieżka absolutna, można zmienić na dowolną relatywną
            'nokaut'=>'/var/www/html/divante/sample_data/nokaut_sample.xml'
        ];

}