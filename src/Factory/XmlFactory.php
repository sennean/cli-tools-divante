<?php
namespace Mapper\Factory;


class XmlFactory
{
    public static function build($name)
    {
        $product = "Mapper\Streamer\Xml\\" . $name . 'Streamer';

        if (class_exists($product, true)) {
            return new $product($name);
        } else {
            throw new Exception("Invalid product type given.");
        }
    }
}