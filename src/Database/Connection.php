<?php
/**
 * Created by PhpStorm.
 * User: karan
 * Date: 06.07.15
 * Time: 17:32
 */
namespace Mapper\Database;
use Mapper\Database\Config;
use PDO;
class Connection
{
    public function getConnection() {

        $db_config = new Config();
        $dns = $db_config->connection_type.':dbname='.$db_config->db_name;'host='.$db_config->db_host;
        try {
            $db_conn = new PDO($dns,$db_config->db_user, $db_config->db_password);
            return $db_conn;
        } catch (\PDOException $e) {
            echo sprintf('Connection failed: %s', $e->getMessage());
            exit;
        }
    }

}