<?php
/**
 * Created by PhpStorm.
 * User: karan
 * Date: 06.07.15
 * Time: 22:31
 */
namespace Mapper\Validator;
interface BaseValidator {
    public function validate($schema);
}