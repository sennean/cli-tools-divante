<?php
/**
 * Created by PhpStorm.
 * User: karan
 * Date: 06.07.15
 * Time: 22:38
 */
namespace Mapper\Validator\Xml;

use Mapper\Config\Config;
use Mapper\Validator\BaseValidator;

class Validator implements BaseValidator
{
    public function validate($schema)
    {
        $schema_name = strtolower($schema);
        $handle = new \DOMDocument();
        $handle->load(Config::$location[$schema_name]);

        if ($handle->schemaValidate (__DIR__ . '/../../Schema/' . $schema_name . '.xsd')) {
            unset($handle);
            return true;
        }
        unset($handle);
        echo 'DOMDocument::schemaValidate() Nieładnie... to nie schemat ' . $schema;
        return false;
    }
}