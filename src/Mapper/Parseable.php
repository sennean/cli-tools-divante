<?php
/**
 * Created by PhpStorm.
 * User: karan
 * Date: 06.07.15
 * Time: 21:53
 */
namespace Mapper\Mapper;
interface Parseable
{

    public function process();

    public function parse();
}