<?php
/**
 * Created by PhpStorm.
 * User: karan
 * Date: 06.07.15
 * Time: 18:44
 */
namespace Mapper\Mapper;

use Mapper\Database\Connection;

abstract class Mapper
{
    protected $db_structure = [
        'name',
        'description',
        'url',
        'created_at',
        'updated_at'

    ];

    public function readSource($source)
    {

    }

    public function saveData($data)
    {
    }
}