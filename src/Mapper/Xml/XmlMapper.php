<?php
/**
 * Created by PhpStorm.
 * User: karan
 * Date: 06.07.15
 * Time: 19:17
 */
namespace Mapper\Mapper\Xml;

use Mapper\Factory\XmlFactory;
use Mapper\Mapper\Mapper;
use Mapper\Mapper\Parseable;
use Mapper\Validator\Xml\Validator;


class XmlMapper extends Mapper implements Parseable
{

    protected $company;
    protected $validate;

    public function __construct($company)
    {
        $this->company = $company;
    }

    public function process()
    {
        $handle = XmlFactory::build($this->company);
        $handle->parse();
    }

    public function setValidation($validation)
    {
        $this->validate = $validation;
    }

    public function parse()
    {
        if ($this->validate) {
            $validator = new Validator();

            if (!$validator->validate($this->company)) {
                return;
            };
        }
        $this->process();

    }


}