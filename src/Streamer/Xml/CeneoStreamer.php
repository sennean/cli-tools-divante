<?php
/**
 * Created by PhpStorm.
 * User: karan
 * Date: 08.07.15
 * Time: 16:50
 */
namespace Mapper\Streamer\Xml;

use Mapper\Config\Config;
use Mapper\Streamer\Streamer;

class CeneoStreamer extends Streamer
{
    protected $schema;

    public function __construct($name)
    {
        $prefix = strtolower($name);
        $this->schema = Config::$schema[$prefix];
        $location = \Mapper\Config\Config::$location;

        parent::__construct($location[$prefix]);
    }

    public function processNode($xmlString, $elementName, $nodeIndex)
    {

        $xml = simplexml_load_string($xmlString);

        $this->values['url'] = $xml->xpath($this->schema['url'])[0]->url;
        $this->values['description'] = $xml->xpath($this->schema['description']);
        $this->values['name'] = $xml->xpath($this->schema['name']);

        return true;
    }

    public function chunkCompleted()
    {
        $this->databaseSave(
            $this->values['name'][0],
            $this->values['description'][0],
            $this->values['url'][0]
        );
    }
}

