<?php
/**
 * Created by PhpStorm.
 * User: karan
 * Date: 08.07.15
 * Time: 16:50
 */
namespace Mapper\Streamer\Xml;

use Mapper\Config\Config;
use Mapper\Streamer\Streamer;

class NokautStreamer extends Streamer
{

    protected $schema;

    public function __construct($name)
    {

        $prefix = strtolower($name);
        $this->schema = Config::$schema[$prefix];
        $location = \Mapper\Config\Config::$location;

        parent::__construct($location[$prefix]);
    }

}