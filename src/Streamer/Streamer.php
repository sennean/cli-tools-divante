<?php
namespace Mapper\Streamer;

use Mapper\Database\Connection;

class Streamer extends \Prewk\XmlStreamer
{

    protected $values;
    protected $connection;
    protected $schema;

    public function init()
    {
        $pdo = new Connection();
        $this->connection = $pdo->getConnection();
    }

    public function processNode($xmlString, $elementName, $nodeIndex)
    {

        $xml = simplexml_load_string($xmlString);
        $this->values['url'] = $xml->xpath($this->schema['url']);
        $this->values['description'] = $xml->xpath($this->schema['description']);
        $this->values['name'] = $xml->xpath($this->schema['name']);
        return true;
    }

    protected function databaseSave($name, $desc, $url)
    {

        $sql = 'INSERT INTO products
                (name, description, url, created_at, updated_at)
                VALUES ("'
            . (string)$name
            . '","'
            . (string)$desc
            . '","'
            . (string)$url
            . '", "'
            . date("Y-m-d H:i:s")
            . '","'
            . date("Y-m-d H:i:s")
            . '")
                ON DUPLICATE KEY
                UPDATE name="'
            . (string)$name
            . '", description="'
            . (string)$desc
            . '", updated_at="'
            . date("Y-m-d H:i:s")
            . '"';
        $this->connection->exec($sql);
    }

    public function chunkCompleted()
    {

        if (is_array($this->values['url'])) {
            $times = count($this->values['url']);
            for ($i = 0; $i < $times; $i++) {
                $this->databaseSave(
                    $this->values['name'][$i],
                    $this->values['description'][$i],
                    $this->values['url'][$i]
                );
            }
        }

    }
}
