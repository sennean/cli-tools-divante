<?php
/**
 * Created by PhpStorm.
 * User: karan
 * Date: 06.07.15
 * Time: 17:44
 */
namespace Mapper;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mapper\Mapper\Xml\XmlMapper;

class Crawler extends Command
{
    protected function configure()
    {
        $this
            ->setName('crawl:xml')
            ->setDescription("crawl by xml files - example crawl:xml 'Ceneo'")
            ->addArgument(
                'company',
                InputArgument::REQUIRED,
                'Z jakiej porównywarki pobrać dane?'
            )
            ->addArgument(
                'validate',
                InputArgument::OPTIONAL,
                'czy ma nastąpić sprawdzenie zgodności ze schematem (bardzo obciąża system)'
            );

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $company = $input->getArgument('company');

        $xml = new XmlMapper($company);

        if ($input->getArgument('validate')) {
            $xml->setValidation($input->getArgument('validate'));
        };
        $xml->parse();

        $output->writeln('Done');
    }

}