<?php
require_once __DIR__ . '/vendor/autoload.php';
use Symfony\Component\Console\Application;
use Mapper\Crawler;

$console = new Application();
$console->add(new Crawler());
$console->run();